<?php declare(strict_types=1);

namespace RaakRdam\KunstmaanApiBundle\Entity;

use RaakRdam\KunstmaanApiBundle\Model;

interface PagePartsModelInterface
{
    /**
     * @return Model\PagePartsEntityInterface
     */
    public function getModel(): Model\PagePartsEntityInterface;
}
