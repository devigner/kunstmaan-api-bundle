<?php declare(strict_types=1);

namespace RaakRdam\KunstmaanApiBundle\Entity\PageParts;

use Kunstmaan\PagePartBundle\Entity\AbstractPagePart as KumaAbstractPagePart;

/**
 * AbstractPagePart
 */
abstract class AbstractPagePart extends KumaAbstractPagePart
{
}
