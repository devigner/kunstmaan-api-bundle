run-copy-from-project:
	cp -Rcp ../../connekt/lean-and-green/backend/vendor/raakrdam/kunstmaan-api-bundle/src ./

run-copy-to-project:
	cp -Rcp ./src ../../connekt/lean-and-green/backend/vendor/raakrdam/kunstmaan-api-bundle/

run-php:
	docker run --rm -it -v `pwd`:/app registry.gitlab.com/raakrdam/docker-hub/symfony/php-fpm-alpine:7.3.5 bash
